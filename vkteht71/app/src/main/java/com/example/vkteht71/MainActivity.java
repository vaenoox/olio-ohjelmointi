package com.example.vkteht71;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    TextView text;
    TextView edit;
    TextView kirjoitettava;
    Context context = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.tulostushello();
        text = (TextView) findViewById(R.id.textView);
        kirjoitettava = (TextView) findViewById(R.id.editText2);
        edit = (TextView) findViewById(R.id.editText);
        context = MainActivity.this;
        edit.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                text.setText(edit.getText());
            }
        });
    }
    public void readFile(View v) {
        try {
            InputStream ins = context.openFileInput(kirjoitettava.getText().toString()+".txt");

            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";

            while ((s=br.readLine()) != null) {
                System.out.println(s);
                text.setText(s);
            }
            ins.close();

        } catch (IOException e) {
            Log.e("IOException", "Virhe Syötteessä");
        } finally {
            System.out.println("Luettu");
        }
    }
    public void writeFile(View v) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(kirjoitettava.getText().toString()+".txt", Context.MODE_PRIVATE));
            String s = edit.getText().toString();
            osw.write(s);
            osw.close();

        } catch (IOException e) {
            Log.e("IOException", "Virhe Syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }
    }
}
