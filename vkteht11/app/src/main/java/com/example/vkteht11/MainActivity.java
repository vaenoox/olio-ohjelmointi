package com.example.vkteht11;

import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    float TextSize;
    int TextWidth;
    int TextHeight;
    int RowCount;
    EditText Size;
    EditText Width;
    EditText Height;
    EditText Rows;
    EditText LongText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Size = findViewById(R.id.FontSize);
        Width = findViewById(R.id.TextWidth);
        Height = findViewById(R.id.TextHeight);
        Rows = findViewById(R.id.RowCount);
        LongText = findViewById(R.id.editText);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TextFragment()).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment()).commit();
                break;
            case R.id.nav_text:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TextFragment()).commit();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void SaveSettings(View v){
        //TextSize = Integer.parseInt(Size.getText().toString());
        TextHeight = Integer.parseInt(Height.getText().toString());
        TextWidth = Integer.parseInt(Width.getText().toString());
        RowCount = Integer.parseInt(Rows.getText().toString());
        if(TextSize > 0) {
            LongText.setTextSize(TextSize);
        }
        if(TextHeight > 0) {
            LongText.setHeight(TextHeight);
        }
        if(TextWidth > 0) {
            LongText.setWidth(TextWidth);
        }
        if(RowCount > 0) {

        }
    }
}
