package com.example.vkteht9;

import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public abstract class MainActivity extends AppCompatActivity {
    ProgressBar progressBar;
    TextView pvm;
    TextView HakuAika2;
    TextView HakuAika1;
    Context context = null;
    Spinner spinner;

    TeatteritLista TL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        progressBar = findViewById(R.id.progressBar);
        HakuAika1 = findViewById(R.id.HakuAika1);
        HakuAika2 = findViewById(R.id.HakuAika2);
        pvm = findViewById(R.id.pvm);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        TL = TeatteritLista.getInstance();
        TL.TeatteriNimet();
    }

    public void readAreasXML (View v) {
        ArrayList<String> valiaikaisName = new ArrayList();
        ArrayList<String> valiaikaisID = new ArrayList();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document docArea = null;
            docArea = builder.parse("https://www.finnkino.fi/xml/TheatreAreas/");
            docArea.getDocumentElement().normalize();
            System.out.println("RootElement: " + docArea.getDocumentElement().getNodeName());
            NodeList nlistArea = docArea.getDocumentElement().getElementsByTagName("TheatreArea");
            for (int i = 0; i < nlistArea.getLength(); i++){

                System.out.print("ID: ");
                String ID = docArea.getElementsByTagName("ID").item(0).getTextContent();
                System.out.println(ID);
                System.out.print("Name: ");
                String Name = docArea.getElementsByTagName("Name").item(0).getTextContent();
                System.out.println(Name);
                valiaikaisID.add(ID);
                valiaikaisName.add(Name);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("#### DONE ####");
        }
        TL.teatteriList(valiaikaisID, valiaikaisName);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, valiaikaisName);
        spinner.setAdapter(dataAdapter);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // Your code to do something with the selected item
                if (TL.Lista.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Jokin meni pieleen yritä uudestaan.", Toast.LENGTH_LONG).show();
                } else {
                    System.out.println("Tämähän toimii ... ... ehkä?!!");

                }
            }
        });
    }
}




