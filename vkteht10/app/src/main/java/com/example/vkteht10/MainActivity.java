package com.example.vkteht10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    WebView web;
    EditText SearchField;
    ImageButton NextButton;
    ImageButton PreviousButton;
    ImageButton RefreshButton;
    ImageButton SearchButton;
    Button Hello;
    String SearchUrl;
    int index = 0;
    ArrayList<String> UrlList;
    String valiaikainen;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SearchField = findViewById(R.id.SearchField);
        NextButton = findViewById(R.id.NextButton);
        PreviousButton = findViewById(R.id.PreaviousButton);
        RefreshButton = findViewById(R.id.RefreshButton);
        SearchButton = findViewById(R.id.SearchButton);
        Hello = findViewById(R.id.HelloWorldButton);
        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl("file:///android_asset/index.html");
        UrlList = new ArrayList<String>();
    }

    //Hello world script
    public void HelloWorld(View v) {
        web.evaluateJavascript("javascript:shoutOut()", null);
    }

    //Moi maailma script
    public void MoiMaailma(View v) {
        web.evaluateJavascript("javascript:initialize()", null);
    }

    //Search Button function web search
    public void SearchWebPage(View v) {
        SearchUrl = "https://"+SearchField.getText().toString();
        web.loadUrl(SearchUrl);
        UrlList.add(SearchUrl);
        System.out.println(UrlList+"; "+UrlList.indexOf(SearchUrl));
    }

    //Next Button
    public void NextWebPage(View v) {
        index = UrlList.indexOf("https://"+SearchField.getText());
        if (index+1 <= UrlList.size()-1) {
            index = index+1;
            web.loadUrl(UrlList.get(index));
            valiaikainen = UrlList.get(index);
            valiaikainen = valiaikainen.replace("https://", "");
            SearchField.setText(valiaikainen);
        } else {
            System.out.println("Lista on lopussa");
        }
    }

    //Previous Button
    public void PreviousWebPage(View v) {
        index = UrlList.indexOf("https://"+SearchField.getText());
        if (index-1 >= 0) {
            index = index-1;
            web.loadUrl(UrlList.get(index));
            valiaikainen = UrlList.get(index);
            valiaikainen = valiaikainen.replace("https://", "");
            SearchField.setText(valiaikainen);
        } else {
            System.out.println("Lista on alussa");
        }
    }

    //Refresh Button
    public void RefreshCurrentPage(View v) {
        web.loadUrl(web.getUrl());
    }
}
