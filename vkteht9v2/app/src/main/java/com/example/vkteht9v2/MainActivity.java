package com.example.vkteht9v2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.StrictMode;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.*;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    TextView pvm;
    TextView HakuAika2;
    TextView HakuAika1;
    TextView TextScroll;
    EditText ElokuvanNimi;
    Context context;
    ArrayList<String> valiaikaisName;
    ArrayList<String> valiaikaisID;
    int nappiapainettu;
    String paivamaara = "";
    String tunti;
    String minuutti;
    String KellonAika1;
    String KellonAika2;
    String alue_ja_teatteri;
    String kuukausi;
    String vuosi;
    String paiva;
    int TeatteriID = 0;
    ElokuvatLista EL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        HakuAika1 = findViewById(R.id.HakuAika1);
        HakuAika2 = findViewById(R.id.HakuAika2);
        ElokuvanNimi = findViewById(R.id.editText);
        TextScroll = findViewById(R.id.textViewScroll);
        TextScroll.setMovementMethod(new ScrollingMovementMethod());
        pvm = findViewById(R.id.pvm);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        EL = ElokuvatLista.getInstance();
        readAreasXML();

        //DATE_PICKER //
        ImageButton pvmButton = findViewById(R.id.pvmButton);
        pvmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        //TIME_PICKER_HakuAika1
        ImageButton Hakuaika1Button = findViewById(R.id.HakuAika1Button);
        Hakuaika1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timepicker1 = new TimePickerFragment();
                timepicker1.show(getSupportFragmentManager(), "time picker 1");
                nappiapainettu = 0;
            }
        });

        //TIME_PICKER_HakuAika2
        ImageButton Hakuaika2Button = findViewById(R.id.HakuAika2Button);
        Hakuaika2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timepicker2 = new TimePickerFragment();
                timepicker2.show(getSupportFragmentManager(), "time picker 2");
                nappiapainettu = 1;
            }
        });


        //SPINNERI
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, valiaikaisName);
        spinner.setAdapter(dataAdapter);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                alue_ja_teatteri = valiaikaisName.get(0);
                System.out.println(alue_ja_teatteri);
            }
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // Your code to do something with the selected item
                if (valiaikaisName.isEmpty()) {
                    System.out.println("JOTAIN ON NYT PAHASTI VIALLA!!!!");
                } else {
                    alue_ja_teatteri = valiaikaisName.get(pos);
                    System.out.println(alue_ja_teatteri);
                    TeatteriID = Integer.parseInt(valiaikaisID.get(pos));
                    System.out.println(TeatteriID);
                }
            }
        });
    }

    //AREASXML_LUKU
    public void readAreasXML() {
        valiaikaisName = new ArrayList();
        valiaikaisID = new ArrayList();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlAreas = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document docArea = builder.parse(urlAreas);
            docArea.getDocumentElement().normalize();
            System.out.println("RootElement: " + docArea.getDocumentElement().getNodeName());
            NodeList nlistArea = docArea.getDocumentElement().getElementsByTagName("TheatreArea");
            for (int i = 0; i < nlistArea.getLength(); i++) {
                System.out.print("ID: ");
                String ID = docArea.getElementsByTagName("ID").item(i).getTextContent();
                System.out.println(ID);
                System.out.print("Name: ");
                String Name = docArea.getElementsByTagName("Name").item(i).getTextContent();
                System.out.println(Name);
                valiaikaisID.add(ID);
                valiaikaisName.add(Name);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            System.out.println("######DONE######");
        }
    }

    //KAIKKIEN_ELOKUVIENHAKU
    public void readMoviesXML(View v) {
        EL.Lista.clear();
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (TeatteriID == 1029) {
            for (int i = 2; i<valiaikaisID.size(); i++) { //TODO korjaa skipped frames ja alue dublikaatit (Helsinki, Tampere ,yms)
                if (valiaikaisName.get(i).contains(":")) {
                    Document docElokuvat = null;
                    try {
                        docElokuvat = builder.parse("https://www.finnkino.fi/xml/Schedule/?area=" + valiaikaisID.get(i) + "&dt=" + paivamaara);
                        docElokuvat.getDocumentElement().normalize();
                        NodeList nlistArea = docElokuvat.getDocumentElement().getElementsByTagName("Show");
                        for (int u = 0; u < nlistArea.getLength(); u++) {
                            String Nimi = docElokuvat.getElementsByTagName("Title").item(u).getTextContent();
                            String Aika = docElokuvat.getElementsByTagName("dttmShowStart").item(u).getTextContent();
                            String Paikka = docElokuvat.getElementsByTagName("Theatre").item(u).getTextContent();
                            System.out.println(Nimi+", "+Paikka+", "+Aika);
                            EL.ElokuviaNaytossa(Nimi, Aika, Paikka);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SAXException e) {
                        e.printStackTrace();
                    }
                }
            }

        } else {
            Document docElokuvat = null;
            try {
                docElokuvat = builder.parse("https://www.finnkino.fi/xml/Schedule/?area="+TeatteriID+"&dt="+paivamaara);
                docElokuvat.getDocumentElement().normalize();
                NodeList nlistArea = docElokuvat.getDocumentElement().getElementsByTagName("Show");
                for (int i = 0; i < nlistArea.getLength(); i++) {
                    String Nimi = docElokuvat.getElementsByTagName("Title").item(i).getTextContent();
                    String Aika = docElokuvat.getElementsByTagName("dttmShowStart").item(i).getTextContent();
                    String Paikka = docElokuvat.getElementsByTagName("Theatre").item(i).getTextContent();
                    System.out.println(Nimi+", "+Paikka+", "+Aika);
                    EL.ElokuviaNaytossa(Nimi, Aika, Paikka);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
        TextScroll.setText("");
        /*for (int i=0 ; i<EL.Lista.size(); i++) {
            TextScroll.append(EL.Lista.get(i).getLocation()+"\n"+EL.Lista.get(i).getName()+", "+EL.Lista.get(i).getTime()+"\n\n");
        }*/
        String elokuva = ElokuvanNimi.getText().toString();
        System.out.println(elokuva);
        if (KellonAika1.length() != 0) {
            for (int i=0; i<EL.Lista.size(); i++) {
                String dateString = EL.Lista.get(i).getTime();
                String[] separated = dateString.split("T");
                String aikajaettu = separated[1].replace(":", "");

                Long aika = Long.parseLong(aikajaettu);
                System.out.println("alkuaika: " + Long.parseLong(KellonAika1)+ "  elokuvan aika: " + aika);
                if (Long.parseLong(KellonAika1) > aika) {
                    EL.PoistaElokuva(i--);
                }
            }
        }
        if (KellonAika2.length() != 0) {
            for (int i=0; i<EL.Lista.size(); i++) {
                String dateString = EL.Lista.get(i).getTime();
                String[] separated = dateString.split("T");
                String aikajaettu = separated[1].replace(":", "");
                int aika = Integer.parseInt(aikajaettu);
                if (Integer.parseInt(KellonAika2) < aika) {
                    EL.PoistaElokuva(i--);
                }
            }
        }
        if (elokuva.length() == 0) {
            for (int i=0 ; i<EL.Lista.size(); i++) {
                TextScroll.append(EL.Lista.get(i).getLocation()+"\n"+EL.Lista.get(i).getName()+", "+EL.Lista.get(i).getTime()+"\n\n");
            }
        } else {
            for (int i = 0; i < EL.Lista.size(); i++) {
                if ((EL.Lista.get(i).getName()).contains(elokuva)) {
                    TextScroll.append(EL.Lista.get(i).getLocation() + "\n" + EL.Lista.get(i).getName() + ", " + EL.Lista.get(i).getTime() + "\n\n");
                }
            }
        }
    }

    //DATE_PICKER //
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        TextView pvm = (TextView) findViewById(R.id.pvm);
        month = month + 1;
        vuosi = Integer.toString(year);
        if (month < 10) {
            kuukausi = "0" + month;
        } else {
            kuukausi = Integer.toString(month);
        }
        if (dayOfMonth < 10) {
            paiva = "0" + dayOfMonth;
        } else {
            paiva = Integer.toString(dayOfMonth);
        }
        paivamaara = paiva+"."+kuukausi+"."+vuosi;
        pvm.setText(paivamaara);
    }

    //TIME_PICKER//TODO TIMEPICKER tulostus oikeaan muotoon HH:MM
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (hourOfDay < 10){
            tunti = "0"+ hourOfDay;
        } else {
            tunti = Integer.toString(hourOfDay);
        }
        if (minute < 10) {
            minuutti = "0" + minute;
        } else {
            minuutti = Integer.toString(minute);
        }
        if (nappiapainettu == 0) {
            TextView HakuAika1 = findViewById(R.id.HakuAika1);
            HakuAika1.setText(tunti + ":" + minuutti);
            KellonAika1 = tunti+minuutti+"00";
        } if (nappiapainettu == 1) {
            TextView HakuAika2 = findViewById(R.id.HakuAika2);
            HakuAika2.setText(tunti + ":" + minuutti);
            KellonAika2 = tunti+minuutti+"00";
        }
    }
}
