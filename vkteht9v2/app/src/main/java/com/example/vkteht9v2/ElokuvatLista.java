package com.example.vkteht9v2;

import java.util.ArrayList;

public class ElokuvatLista {
    private String Name;
    private String Location;
    private String Time;
    Elokuva temp;
    ArrayList<Elokuva> Lista;

    private static ElokuvatLista instance = null;
    private static  ElokuvatLista EL = new ElokuvatLista();
    private ElokuvatLista() {
        Lista = new ArrayList();
    }

    public static ElokuvatLista getInstance() {
        if (instance == null) {
            instance = new ElokuvatLista();
        }
        else{
            System.out.println("Instance already exists");
        }
        return instance;
    }

    public void  ElokuviaNaytossa(String Nimi, String Paikka, String Aika) {
            Name = Nimi;
            Location = Paikka;
            Time = Aika;
            temp = new Elokuva(Nimi, Aika, Paikka);
            Lista.add(temp);
    }
    public void PoistaElokuva(int i) {
        Lista.remove(i);
    }
}

