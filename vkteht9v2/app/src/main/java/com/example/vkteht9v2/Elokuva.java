package com.example.vkteht9v2;

public class Elokuva {
    private String Name;
    private String Location;
    private String Time;
    public Elokuva(String Nimi, String Paikka, String Aika) {
        Name = Nimi;
        Location = Paikka;
        Time = Aika;
    }

    public String getName() {
        return Name;
    }

    public String getLocation(){
        return Location;
    }

    public String getTime(){
        return Time;
    }
}
