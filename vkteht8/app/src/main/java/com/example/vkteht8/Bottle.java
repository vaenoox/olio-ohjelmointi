package com.example.vkteht8;

public class Bottle {
    private String name;
    private String drink;
    private float size;
    private float price;
    public Bottle(String nimi, float koko, float hinta) {
        name = nimi;
        size = koko;
        price = hinta;
    }
    public String getName(){
        return name;
    }
    public float getSize(){
        return size;
    }
    public float getPrice(){
        return price;
    }
    @Override
    public String toString() {
         String s = String.format("%.2f", price);
         String pullo = name+", "+size+"l, "+s+"€";
         return pullo;
    }
}
