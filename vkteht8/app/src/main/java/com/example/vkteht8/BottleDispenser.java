package com.example.vkteht8;

import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Scanner;

public class BottleDispenser {

    private int bottles;
    private String name;
    public float money, size, price;
    Bottle temp;
    TextView konsoli;

    ArrayList<Bottle> lista = new ArrayList();

    private static BottleDispenser BD = new BottleDispenser();

    private BottleDispenser() {
        bottles = 6;
        money = 0f;
        temp = new Bottle("Pepsi Max", 0.5f, 1.8f);
        lista.add(temp);
        temp = new Bottle("Pepsi Max", 1.5f, 2.2f);
        lista.add(temp);
        temp = new Bottle("Coca-Cola Zero", 0.5f, 2.0f);
        lista.add(temp);
        temp = new Bottle("Coca-Cola Zero", 1.5f, 2.5f);
        lista.add(temp);
        temp = new Bottle("Fanta Zero", 0.5f, 1.95f);
        lista.add(temp);
        temp = new Bottle("Fanta Zero", 0.5f, 1.95f);
        lista.add(temp);
    }
    public static BottleDispenser getInstance() {
        return BD;
    }
    public void konsolisyote(TextView console) {
        konsoli = console;
    }

    public void addMoney(float lisaa) {
        money = (money + lisaa);
        String s = String.format("%.2f", lisaa);
        konsoli.setText("Klink! Lisäsit laitteeseen " + s + "€");
    }

    public String buyBottle(int pos) {
            money -= lista.get(pos).getPrice();
            konsoli.setText("KACHUNK! "+ lista.get(pos).getName()+" tipahti masiinasta! Kuitti tulostettu.");
            lista.remove(pos);
            return "ei";

    }
    public void returnMoney() {
        String s = String.format("%.2f", money);
        konsoli.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + s + "€");
        money = 0;
    }
}
