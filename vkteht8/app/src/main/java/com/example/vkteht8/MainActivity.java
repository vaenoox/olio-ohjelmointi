package com.example.vkteht8;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    TextView Lisaarahaa;
    TextView console;
    TextView Rahaakoneessa;
    Context context = null;
    SeekBar HintaSlider;
    float lisattavasumma;
    int ostettavatuote;

    BottleDispenser BD = BottleDispenser.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Lisaarahaa = (TextView) findViewById(R.id.Lisaarahaa);
        console = (TextView) findViewById(R.id.console);
        Rahaakoneessa = (TextView) findViewById(R.id.Rahaakoneessa);
        context = MainActivity.this;
        HintaSlider = (SeekBar) findViewById(R.id.HintaSlider);
        final String OstettavaTuote;


        //Spinnerin määrittely
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, BD.lista);
        spinner.setAdapter(dataAdapter);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // Your code to do something with the selected item
                if (BD.lista.isEmpty() == true){
                    console.setText("Tuotteet ovat loppu.");
                } else {
                    String tuote = BD.lista.get(pos).getName();
                    float koko = BD.lista.get(pos).getSize();
                    String tilavuus = String.format("%.2f", koko);
                    float hinta = BD.lista.get(pos).getPrice();
                    String euroja = String.format("%.2f", hinta);
                    console.setText(tuote + ", " + tilavuus + ", " + euroja + "€");
                    ostettavatuote = pos;
                }
            }
        });


        //SEEKBAR MÄÄRITTELY
        HintaSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Lisaarahaa.setText("Lisää rahaa: " + progress + "€");
                lisattavasumma = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
    public void ostaPullo(View v) {
        if (BD.lista.isEmpty()) {
            console.setText("Tuotteet ovat loppu.");
            Toast.makeText(getApplicationContext(), "Tuotteet ovat loppu.", Toast.LENGTH_LONG).show();
        } else {
            if (BD.lista.get(ostettavatuote).getPrice() < BD.money) {
                try {
                    OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput("kuitti.txt", Context.MODE_PRIVATE));
                    String s = console.getText().toString();
                    osw.write(s);
                    osw.close();

                } catch (IOException e) {
                    Log.e("IOException", "Virhe Syötteessä");
                } finally {
                    System.out.println("Kirjoitettu");
                }
                BD.buyBottle(ostettavatuote);
                String rahaanyt = String.format("%.2f", BD.money);
                Rahaakoneessa.setText(rahaanyt + "€");
            } else {
                console.setText("Lisää rahaa.");
                Toast.makeText(getApplicationContext(), "Lisää rahaa.", Toast.LENGTH_LONG).show();
            }
        }
    }
    public void palautarahat(View v){
        BD.konsolisyote(console);
        BD.returnMoney();
        String rahaanyt = String.format("%.2f", BD.money);
        Rahaakoneessa.setText(rahaanyt + "€");
    }
    public void lisataanrahaa(View v) {
        BD.konsolisyote(console);
        BD.addMoney(lisattavasumma);
        String rahaanyt = String.format("%.2f", BD.money);
        Rahaakoneessa.setText(rahaanyt + "€");
    }
}